package com.ruoyi.quartz.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * 定时任务调度日志表 sys_job_log
 *
 * @author wintersnow
 * @since 1.0  2020-12-14
 */
@Entity
@Table(name = "sys_job_log")
@Data
public class SysJobLog extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @Excel(name = "日志序号")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "job_log_id")
    private Long jobLogId;

    /**
     * 任务名称
     */
    @Excel(name = "任务名称")
    @Column(name = "job_name")
    private String jobName;

    /**
     * 任务组名
     */
    @Excel(name = "任务组名")
    @Column(name = "job_group")
    private String jobGroup;

    /**
     * 调用目标字符串
     */
    @Excel(name = "调用目标字符串")
    @Column(name = "invoke_target")
    private String invokeTarget;

    /**
     * 日志信息
     */
    @Excel(name = "日志信息")
    @Column(name = "job_message")
    private String jobMessage;

    /**
     * 执行状态（0正常 1失败）
     */
    @Excel(name = "执行状态", readConverterExp = "0=正常,1=失败")
    @Column(name = "status")
    private String status;

    /**
     * 异常信息
     */
    @Excel(name = "异常信息")
    @Column(name = "exception_info")
    private String exceptionInfo;

    /**
     * 开始时间
     */
    private Date startTime;

    /**
     * 停止时间
     */
    private Date stopTime;


    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "create_time")
    private Date createTime;

}
