package com.ruoyi.common.core.dto;

import lombok.Data;

import java.math.BigInteger;
import java.util.Date;


/**
 * 部门表 sys_dept
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */
@Data
public class SysDeptDto {


    /**
     * 部门ID
     */
    private BigInteger deptId;

    /**
     * 父部门ID
     */
    private BigInteger parentId;

    /**
     * 祖级列表
     */
    private String ancestors;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 显示顺序
     */
    private Integer orderNum;

    /**
     * 负责人
     */
    private String leader;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 部门状态:0正常,1停用
     */
    private Character status;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private Character delFlag;


    /**
     * 创建者
     */

    private String createBy;

    /**
     * 创建时间
     */

    private Date createTime;

    /**
     * 更新者
     */

    private String updateBy;

    /**
     * 更新时间
     */

    private Date updateTime;


}
