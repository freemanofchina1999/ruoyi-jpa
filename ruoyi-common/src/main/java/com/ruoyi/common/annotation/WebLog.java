package com.ruoyi.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 通用的web层日志记录注解
 * 
 * @author 0096001664
 * @since 1.0, 2019-01-16
 */
@Documented
@Retention(RUNTIME)
@Target({ METHOD })
public @interface WebLog {

    /**
     * 自定义日志方法名
     * 
     * @return
     */
    String value() default "";

    /**
     * 是否忽略打印
     * 
     * @return
     */
    boolean ignore() default false;

    /**
     * 是打印返回参数
     * 
     * @return
     */
    boolean logReturn() default false;

}
