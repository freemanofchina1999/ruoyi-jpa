package com.ruoyi.generator.dao;

import com.ruoyi.generator.domain.GenTableColumn;

import java.util.List;

public interface GenTableColumnDaoCustom {

    List<GenTableColumn> findDbTableColumnsByName(String tableName);

}
