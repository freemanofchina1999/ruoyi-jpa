package com.ruoyi.generator.service;

import com.ruoyi.common.utils.code.BusinessBizCode;
import com.ruoyi.generator.dao.GenTableColumnDao;
import com.ruoyi.generator.domain.GenTableColumn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 业务字段 服务层实现
 *
 * @author ruoyi
 */
@Transactional(readOnly = true)
@Service
public class GenTableColumnServiceImpl implements IGenTableColumnService {

    @Autowired
    private GenTableColumnDao genTableColumnDao;

    /**
     * 查询业务字段列表
     *
     * @param tableId 业务字段编号
     * @return 业务字段集合
     */
    @Override
    public List<GenTableColumn> selectGenTableColumnListByTableId(Long tableId) {
        List<GenTableColumn> genTableColumns = genTableColumnDao.findByTableId(tableId);
        return genTableColumns;
    }

    /**
     * 新增业务字段
     *
     * @param genTableColumn 业务字段信息
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int insertGenTableColumn(GenTableColumn genTableColumn) {
        genTableColumn.setCreateTime(new Date());
        genTableColumnDao.save(genTableColumn);
        return BusinessBizCode.OPTION_SUCCESS.getCode();
    }

    /**
     * 修改业务字段
     *
     * @param genTableColumn 业务字段信息
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateGenTableColumn(GenTableColumn genTableColumn) {
        genTableColumn.setUpdateTime(new Date());
        genTableColumnDao.save(genTableColumn);
        return BusinessBizCode.OPTION_SUCCESS.getCode();
    }

    /**
     * 删除业务字段对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int deleteGenTableColumnByIds(String ids) {
        List<Long> listIds = Arrays.asList(ids.split(",")).stream().map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
        genTableColumnDao.deleteByTableIdIn(listIds);
        return BusinessBizCode.OPTION_SUCCESS.getCode();
    }
}
