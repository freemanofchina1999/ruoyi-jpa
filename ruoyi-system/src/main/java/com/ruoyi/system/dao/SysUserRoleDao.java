package com.ruoyi.system.dao;

import com.ruoyi.system.domain.SysUserRole;
import com.ruoyi.system.domain.groupkey.SysUserRoleKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * 用户与角色关联表 数据层
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */
@Repository
public interface SysUserRoleDao extends JpaRepository<SysUserRole, SysUserRoleKey>, JpaSpecificationExecutor<SysUserRole>, SysMenuDaoCustom {

    @Query(value = " select count(1) from sys_user_role where role_id=? ", nativeQuery = true)
    Integer countUserRoleByRoleId(Long roleId);

    void deleteByUserId(Long userId);

    Integer deleteByUserIdIn(List<Long> userIds);

    List<SysUserRole> findByUserId(Long userId);

}
