package com.ruoyi.system.dao;

import com.ruoyi.common.core.domain.entity.SysUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * 用户表 数据层
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */
@Repository
public interface SysUserDao extends JpaRepository<SysUser, Long>, JpaSpecificationExecutor<SysUser>, SysUserDaoCustom {

    Optional<SysUser> findByUserName(String userName);

    Optional<SysUser> findByPhonenumber(String phonenumber);

    Optional<SysUser> findByEmail(String email);

    @Modifying
    @Query(value = " update sys_user set avatar = ?1 where user_name = ?2 ", nativeQuery = true)
    Integer updateUserAvatar(String userName, String avatar);

    Integer deleteByUserIdIn(List<Long> userIds);

}
