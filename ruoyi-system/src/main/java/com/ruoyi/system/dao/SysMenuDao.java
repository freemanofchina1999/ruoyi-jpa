package com.ruoyi.system.dao;


import com.ruoyi.common.core.domain.entity.SysMenu;
import com.ruoyi.common.core.dto.SysMenuDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 菜单表 数据层
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */
@Repository
public interface SysMenuDao extends JpaRepository<SysMenu, Long>, JpaSpecificationExecutor<SysMenu>, SysMenuDaoCustom {

    @Query(value = " select count(1) from sys_menu where parent_id =?1   ", nativeQuery = true)
    Integer hasChildByMenuId(Long menuId);

    List<SysMenu> findByMenuNameAndParentId(String menuName, Long parentId);


}
