package com.ruoyi.system.dao;

import java.util.List;

/**
 * 岗位信息 数据层
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */
public interface SysPostDaoCustom {

    List<Long> findPostListByUserId(Long userId);

}
