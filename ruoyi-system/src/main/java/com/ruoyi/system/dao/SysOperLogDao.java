package com.ruoyi.system.dao;

import com.ruoyi.system.domain.SysOperLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * 操作日志 数据层
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */
@Repository
public interface SysOperLogDao extends JpaRepository<SysOperLog, Long>, JpaSpecificationExecutor<SysOperLog>, SysMenuDaoCustom {

    void deleteByOperIdIn(List<Long> ids);

    @Modifying
    @Query(value = "truncate table sys_oper_log", nativeQuery = true)
    void cleanOperLog();

}
