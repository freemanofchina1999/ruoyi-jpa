package com.ruoyi.system.dao;

import com.ruoyi.common.core.domain.entity.SysUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * 用户表 数据层
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */
public interface SysUserDaoCustom {

    Page<SysUser> findMixPage(SysUser user, Pageable pageable);

}
