package com.ruoyi.system.dao;

import com.ruoyi.system.domain.SysRoleDept;
import com.ruoyi.system.domain.groupkey.SysRoleDeptKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 角色与部门关联表 数据层
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */
@Repository
public interface SysRoleDeptDao extends JpaRepository<SysRoleDept, SysRoleDeptKey> {

    List<SysRoleDept> findByRoleId(Long roleId);

    void deleteByRoleId(Long roleId);

    void deleteByRoleIdIn(List<Long> roleId);

}
