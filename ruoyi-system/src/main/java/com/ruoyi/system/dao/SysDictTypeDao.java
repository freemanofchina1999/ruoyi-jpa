package com.ruoyi.system.dao;

import com.ruoyi.common.core.domain.entity.SysDictType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * 字典表 数据层
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */
@Repository
public interface SysDictTypeDao extends JpaRepository<SysDictType, Long>, JpaSpecificationExecutor<SysDictType> {

    List<SysDictType> findByDictType(String dictType);

}
