package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.core.page.TableSupport;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.code.BusinessBizCode;
import com.ruoyi.common.utils.sql.SqlUtil;
import com.ruoyi.system.dao.SysNoticeDao;
import com.ruoyi.system.domain.SysNotice;
import com.ruoyi.system.service.ISysNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;


/**
 * 公告 服务层实现
 *
 * @author wintersnow
 * @since 1.0  2020-12-12
 */
@Transactional(readOnly = true)
@Service
public class SysNoticeServiceImpl implements ISysNoticeService {

    @Autowired
    private SysNoticeDao noticeDao;

    /**
     * 查询公告信息
     *
     * @param noticeId 公告ID
     * @return 公告信息
     */
    @Override
    public SysNotice selectNoticeById(Long noticeId) {
        return noticeDao.findById(noticeId).orElse(new SysNotice());
    }

    /**
     * 查询公告列表
     *
     * @param req 公告信息
     * @return 公告集合
     */
    @Override
    public Page<SysNotice> selectNoticeList(SysNotice req) {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        if (StringUtils.isNotNull(pageDomain.getPageNum()) && StringUtils.isNotNull(pageDomain.getPageSize())) {
            String orderBy = SqlUtil.escapeOrderBySql(pageDomain.getOrderBy());
        }
        Specification<SysNotice> example = new Specification<SysNotice>() {
            private static final long serialVersionUID = 1L;

            @Override
            public Predicate toPredicate(Root<SysNotice> root,
                                         CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> list = new ArrayList<>();
                if (StringUtils.isNoneBlank(req.getNoticeTitle())) {
                    Predicate pre = cb.like(root.get("noticeTitle").as(String.class), "%" + req.getNoticeTitle() + "%");
                    list.add(pre);
                }
                if (StringUtils.isNoneBlank(req.getNoticeType())) {
                    Predicate pre = cb.equal(root.get("noticeType").as(String.class), req.getNoticeType());
                    list.add(pre);
                }
                if (StringUtils.isNoneBlank(req.getCreateBy())) {
                    Predicate pre = cb.like(root.get("createBy").as(String.class), "%" + req.getCreateBy() + "%");
                    list.add(pre);
                }
                if (list.isEmpty()) {
                    return null;
                }
                return cb.and(list.toArray(new Predicate[0]));
            }
        };
        Pageable pageable = PageRequest.of(pageDomain.getPageNo(), pageDomain.getPageSize(), Sort.Direction.DESC, Optional.ofNullable(pageDomain.getOrderByColumn()).orElse("createTime"));
        Page<SysNotice> page = noticeDao.findAll(example, pageable);
        return page;
    }

    /**
     * 新增公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int insertNotice(SysNotice notice) {
        notice.setCreateTime(new Date());
        noticeDao.save(notice);
        return BusinessBizCode.OPTION_SUCCESS.getCode();
    }

    /**
     * 修改公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateNotice(SysNotice notice) {
        noticeDao.save(notice);
        return BusinessBizCode.OPTION_SUCCESS.getCode();
    }

    /**
     * 删除公告对象
     *
     * @param noticeId 公告ID
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int deleteNoticeById(Long noticeId) {
        noticeDao.deleteById(noticeId);
        return BusinessBizCode.OPTION_SUCCESS.getCode();
    }

    /**
     * 批量删除公告信息
     *
     * @param noticeIds 需要删除的公告ID
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int deleteNoticeByIds(Long[] noticeIds) {
        noticeDao.deleteByNoticeIdIn(Arrays.asList(noticeIds));
        return BusinessBizCode.OPTION_SUCCESS.getCode();
    }
}
