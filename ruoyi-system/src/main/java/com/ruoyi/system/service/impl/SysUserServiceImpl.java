package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.core.page.TableSupport;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.code.BusinessBizCode;
import com.ruoyi.common.utils.code.CommonBizCode;
import com.ruoyi.system.dao.*;
import com.ruoyi.system.domain.SysPost;
import com.ruoyi.system.domain.SysUserPost;
import com.ruoyi.system.domain.SysUserRole;
import com.ruoyi.system.service.ISysConfigService;
import com.ruoyi.system.service.ISysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 用户 业务层处理
 *
 * @author wintersnow
 * @since 1.0  2020-12-12
 */
@Slf4j
@Transactional(readOnly = true)
@Service
public class SysUserServiceImpl implements ISysUserService {

    @Autowired
    private SysUserDao userDao;

    @Autowired
    private SysRoleDao roleDao;

    @Autowired
    private SysPostDao postDao;

    @Autowired
    private SysUserRoleDao userRoleDao;

    @Autowired
    private SysUserPostDao userPostDao;

    @Autowired
    private SysDeptDao deptDao;

    @Autowired
    private ISysConfigService configService;


    /**
     * 根据条件分页查询用户列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    @Override
    @DataScope(deptAlias = "d", userAlias = "u")
    public Page<SysUser> selectUserList(SysUser user) {
        PageDomain pageReq = TableSupport.buildPageRequest();
        Page<SysUser> mixPage = userDao.findMixPage(user, PageRequest.of(pageReq.getPageNum() - 1, pageReq.getPageSize()));
        return mixPage;
    }

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    @Override
    public SysUser selectUserByUserName(String userName) {
        Optional<SysUser> userOptional = userDao.findByUserName(userName);
        if (!userOptional.isPresent()) {
            return null;
        }
        SysUser sysUser = formatUser(userOptional.get());
        return sysUser;
    }

    private SysUser formatUser(SysUser sysUser){
        List<SysUserRole> userRoles = userRoleDao.findByUserId(sysUser.getUserId());
        if (!userRoles.isEmpty()) {
            List<Long> roleIds = userRoles.stream().map(r -> r.getRoleId()).collect(Collectors.toList());
            List<SysRole> sysRoles = roleDao.findByRoleIdIn(roleIds);
            sysUser.setRoleIds(roleIds.stream().toArray(Long[]::new));
            sysUser.setRoles(sysRoles);
        }
        Optional<SysDept> deptOptional = deptDao.findById(sysUser.getDeptId());
        sysUser.setDept(deptOptional.orElse(new SysDept()));
        List<SysUserPost> sysUserPosts = userPostDao.findByUserId(sysUser.getUserId());
        if (!sysUserPosts.isEmpty()) {
            Long[] postIds = sysUserPosts.stream().map(p -> p.getPostId()).toArray(Long[]::new);
            sysUser.setPostIds(postIds);
        }
        return sysUser;
    }

    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    @Override
    public SysUser selectUserById(Long userId) {
        Optional<SysUser> userOptional = userDao.findById(userId);
        SysUser sysUser = formatUser(userOptional.get());
        return sysUser;
    }

    /**
     * 查询用户所属角色组
     *
     * @param userName 用户名
     * @return 结果
     */
    @Override
    public String selectUserRoleGroup(String userName) {
        List<SysRole> list = roleDao.findRolesByUserName(userName);
        StringBuffer idsStr = new StringBuffer();
        for (SysRole role : list) {
            idsStr.append(role.getRoleName()).append(",");
        }
        if (StringUtils.isNotEmpty(idsStr.toString())) {
            return idsStr.substring(0, idsStr.length() - 1);
        }
        return idsStr.toString();
    }

    /**
     * 查询用户所属岗位组
     *
     * @param userName 用户名
     * @return 结果
     */
    @Override
    public String selectUserPostGroup(String userName) {
        List<SysPost> list = postDao.findPostsByUserName(userName);
        StringBuffer idsStr = new StringBuffer();
        for (SysPost post : list) {
            idsStr.append(post.getPostName()).append(",");
        }
        if (StringUtils.isNotEmpty(idsStr.toString())) {
            return idsStr.substring(0, idsStr.length() - 1);
        }
        return idsStr.toString();
    }

    /**
     * 校验用户名称是否唯一
     *
     * @param userName 用户名称
     * @return 结果
     */
    @Override
    public String checkUserNameUnique(String userName) {
        Optional<SysUser> optionalSysUser = userDao.findByUserName(userName);
        if (optionalSysUser.isPresent()) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验用户名称是否唯一
     *
     * @param user 用户信息
     * @return
     */
    @Override
    public String checkPhoneUnique(SysUser user) {
        Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
        Optional<SysUser> userOptional = userDao.findByPhonenumber(user.getPhonenumber());
        if (userOptional.isPresent() && userOptional.get().getUserId().longValue() != userId.longValue()) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验email是否唯一
     *
     * @param user 用户信息
     * @return
     */
    @Override
    public String checkEmailUnique(SysUser user) {
        Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
        Optional<SysUser> userOptional = userDao.findByEmail(user.getEmail());
        if (userOptional.isPresent() && userOptional.get().getUserId().longValue() != userId.longValue()) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验用户是否允许操作
     *
     * @param user 用户信息
     */
    @Override
    public void checkUserAllowed(SysUser user) {
        if (StringUtils.isNotNull(user.getUserId()) && user.isAdmin()) {
            throw new CustomException("不允许操作超级管理员用户");
        }
    }

    /**
     * 新增保存用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int insertUser(SysUser user) {
        // 新增用户信息
        user.setDelFlag(CommonBizCode.DelFlag.NO.getCode());
        user.setCreateTime(new Date());
        user.setCreateBy(SecurityUtils.getUsername());
        userDao.save(user);
        // 新增用户岗位关联
        insertUserPost(user);
        // 新增用户与角色管理
        insertUserRole(user);
        return BusinessBizCode.OPTION_SUCCESS.getCode();
    }

    /**
     * 修改保存用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateUser(SysUser user) {
        Long userId = user.getUserId();
        // 删除用户与角色关联
        userRoleDao.deleteByUserId(userId);
        // 新增用户与角色管理
        insertUserRole(user);
        // 删除用户与岗位关联
        userPostDao.deleteByUserId(userId);
        // 新增用户与岗位管理
        insertUserPost(user);
        Optional<SysUser> userOptional = userDao.findById(user.getUserId());
        if(userOptional.isPresent() && StringUtils.isBlank(user.getPassword())){
            user.setPassword(userOptional.get().getPassword());
        }
        userDao.save(user);
        return BusinessBizCode.OPTION_SUCCESS.getCode();
    }

    /**
     * 修改用户状态
     *
     * @param user 用户信息
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateUserStatus(SysUser user) {
        SysUser sysUser = userDao.findById(user.getUserId()).get();
        sysUser.setStatus(user.getStatus());
        sysUser.setUpdateBy(user.getUpdateBy());
        sysUser.setUpdateTime(user.getUpdateTime());
        userDao.save(sysUser);
        return BusinessBizCode.OPTION_SUCCESS.getCode();
    }

    /**
     * 修改用户基本信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateUserProfile(SysUser user) {
        userDao.save(user);
        return BusinessBizCode.OPTION_SUCCESS.getCode();
    }

    /**
     * 修改用户头像
     *
     * @param userName 用户名
     * @param avatar   头像地址
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updateUserAvatar(String userName, String avatar) {
        return userDao.updateUserAvatar(userName, avatar) > 0;
    }

    /**
     * 重置用户密码
     *
     * @param user 用户信息
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int resetPwd(SysUser user) {
        SysUser sysUser = userDao.findById(user.getUserId()).get();
        sysUser.setPassword(user.getPassword());
        sysUser.setUpdateBy(user.getUpdateBy());
        sysUser.setUpdateTime(new Date());
        userDao.save(sysUser);
        return BusinessBizCode.OPTION_SUCCESS.getCode();
    }

    /**
     * 重置用户密码
     *
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int resetUserPwd(String userName, String password) {
        SysUser sysUser = userDao.findByUserName(userName).get();
        sysUser.setPassword(password);
        userDao.save(sysUser);
        return BusinessBizCode.OPTION_SUCCESS.getCode();
    }

    /**
     * 新增用户角色信息
     *
     * @param user 用户对象
     */
    @Transactional(rollbackFor = Exception.class)
    public void insertUserRole(SysUser user) {
        Long[] roles = user.getRoleIds();
        if (StringUtils.isNotNull(roles)) {
            // 新增用户与角色管理
            List<SysUserRole> list = new ArrayList<SysUserRole>();
            for (Long roleId : roles) {
                SysUserRole ur = new SysUserRole();
                ur.setUserId(user.getUserId());
                ur.setRoleId(roleId);
                list.add(ur);
            }
            if (list.size() > 0) {
                userRoleDao.saveAll(list);
            }
        }
    }

    /**
     * 新增用户岗位信息
     *
     * @param user 用户对象
     */
    @Transactional(rollbackFor = Exception.class)
    public void insertUserPost(SysUser user) {
        Long[] posts = user.getPostIds();
        if (StringUtils.isNotNull(posts)) {
            // 新增用户与岗位管理
            List<SysUserPost> list = new ArrayList<SysUserPost>();
            for (Long postId : posts) {
                SysUserPost up = new SysUserPost();
                up.setUserId(user.getUserId());
                up.setPostId(postId);
                list.add(up);
            }
            if (list.size() > 0) {
                userPostDao.saveAll(list);
            }
        }
    }

    /**
     * 通过用户ID删除用户
     *
     * @param userId 用户ID
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int deleteUserById(Long userId) {
        // 删除用户与角色关联
        userRoleDao.deleteByUserId(userId);
        // 删除用户与岗位表
        userPostDao.deleteByUserId(userId);
        userDao.deleteById(userId);
        return BusinessBizCode.OPTION_SUCCESS.getCode();
    }

    /**
     * 批量删除用户信息
     *
     * @param userIds 需要删除的用户ID
     * @return 结果
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int deleteUserByIds(Long[] userIds) {
        for (Long userId : userIds) {
            checkUserAllowed(new SysUser(userId));
        }
        // 删除用户与角色关联
        userRoleDao.deleteByUserIdIn(Arrays.asList(userIds));
        // 删除用户与岗位关联
        userPostDao.deleteByUserIdIn(Arrays.asList(userIds));
        userDao.deleteByUserIdIn(Arrays.asList(userIds));
        return BusinessBizCode.OPTION_SUCCESS.getCode();
    }

    /**
     * 导入用户数据
     *
     * @param userList        用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName        操作用户
     * @return 结果
     */
    @Override
    public String importUser(List<SysUser> userList, Boolean isUpdateSupport, String operName) {
        if (StringUtils.isNull(userList) || userList.size() == 0) {
            throw new CustomException("导入用户数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        String password = configService.selectConfigByKey("sys.user.initPassword");
        for (SysUser user : userList) {
            try {
                // 验证是否存在这个用户
                Optional<SysUser> userOptional = userDao.findByUserName(user.getUserName());
                if (!userOptional.isPresent()) {
                    user.setPassword(SecurityUtils.encryptPassword(password));
                    user.setCreateBy(operName);
                    this.insertUser(user);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、账号 " + user.getUserName() + " 导入成功");
                } else if (isUpdateSupport) {
                    user.setUpdateBy(operName);
                    this.updateUser(user);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、账号 " + user.getUserName() + " 更新成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、账号 " + user.getUserName() + " 已存在");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、账号 " + user.getUserName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new CustomException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }
}
