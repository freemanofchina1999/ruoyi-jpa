package com.ruoyi.system.domain.groupkey;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户和角色关联 sys_user_role 联合主键
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */
@Data
public class SysUserRoleKey implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 角色ID
     */
    private Long roleId;
}
