package com.ruoyi.system.domain.groupkey;

import lombok.Data;

import java.io.Serializable;


/**
 * 角色和菜单关联 sys_role_menu 联合主键
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */

@Data
public class SysRoleMenuKey implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 菜单ID
     */
    private Long menuId;

}
