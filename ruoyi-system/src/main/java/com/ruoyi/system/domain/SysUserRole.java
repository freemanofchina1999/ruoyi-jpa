package com.ruoyi.system.domain;

import com.ruoyi.system.domain.groupkey.SysUserRoleKey;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 用户和角色关联 sys_user_role
 *
 * @author wintersnow
 * @since 1.0  2020-12-11
 */
@Entity
@Table(name = "sys_user_role")
@IdClass(SysUserRoleKey.class)
@Data
public class SysUserRole implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    @Id
    @Column(name = "user_id")
    private Long userId;

    /**
     * 角色ID
     */
    @Id
    @Column(name = "role_id")
    private Long roleId;
}
